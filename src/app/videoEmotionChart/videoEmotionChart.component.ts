import {Component, OnInit} from '@angular/core';
import * as Plyr from 'plyr';
@Component({
  selector: 'videoEmotionChart',
  templateUrl: './videoEmotionChart.component.html',
  styleUrls: ['./videoEmotionChart.component.scss']
})
export class VideoEmotionChart implements OnInit {
  checkedItem: { id: string; }[];
  url: string | ArrayBuffer;
  usrData: ({ id: string; value: string; } | { id: string; value?: undefined; })[];
  mulUserData: { name: string; img: string; userDtls: { id: string; value: string; }[]; }[];
  userEmotionData: { id: string; value: string; }[];
  player: Plyr;
  mulPlayer: Plyr;
  videoFiles: { thumb: string; id: string; }[];
  readioSelected:any;
  videoSources: Plyr.Source[]=[
    {
      src: 'assets/video/original.mp4',
      type: 'video/mp4',
      size: 1440,
    },
  ]

  options: Plyr.Options = {
    controls: [],
    autoplay:true,
    muted:true,
    clickToPlay:false,
    volume:1,
    }
  poster: string;
  mulposter: string;
  readioMulSelected: string;
  mulvideoSources: Plyr.Source[]=[
    {
      src: 'assets/video/original.mp4',
      type: 'video/mp4',
      size: 1440,
    },
  ]
  clipboardColor:any='gainsboro';
  clipboardModalColor:any='gainsboro';
  mulClipboardColor:any='gainsboro';
  plyrBtn:boolean=true;
  mulPlyrBtn:boolean=true;
  preetyCode: string;
  constructor() {}
  ngOnInit() {
    this.plyrBtn=true;
    this.mulPlyrBtn=true;
    this.checkedItem =[
      {
        id:'Identify'
      },
      {
        id:'Age'
      },
      {
        id:'Race'
      },
      {
        id:'Gender'
      },
      {
        id:'Mood'
      }
      
    ];
    this.videoFiles=[
      {
        thumb:'assets/video/thumb/img1.png',
        id:'original'
      },
      {
        thumb:'assets/video/thumb/img1.png',
        id:'1012'
      },
       {
        thumb:'assets/video/thumb/img1.png',
        id:'1013'
      },
       {
        thumb:'assets/video/thumb/img4.png',
        id:'1015'
      }
    ]
    this.usrData =[
      {
        id:'Name',
        value:'Dinesh Kumar Behera'
      },
      {
        id:'Sex',
        value:'Male'
      },
      {
        id:'Race',
        value:'Hispanic'
      },
      {
        id:'Age',
        value:'20-30'
      }
    ]
    this.userEmotionData =[
      {
        id:'Surprised',
        value:'0'
      },
      {
        id:'Angry',
        value:'10'
      },
      {
        id:'Sad',
        value:'20'
      },
      {
        id:'Happy',
        value:'30'
      },
      {
        id:'Neutral',
        value:'40'
      },
      {
        id:'Anxiety',
        value:'80'
      }
    ]
    this.mulUserData =[
      {
        name:'Sujit Kumar Behera',
        img:'assets/video/thumb/img4.png',
        userDtls:[
 {
   id:'Sex',
   value:'Male'
 },
 {
   id:'Race',
   value:'Asian'
 },
 {
   id:'Age',
   value:'40-45'
 },
 {
   id:'Surprised',
   value:'0'
 },
 {
   id:'Angry',
   value:'30'
 },
 {
   id:'Sad',
   value:'20'
 },
 {
   id:'Happy',
   value:'20'
 },
 {
   id:'Neutral',
   value:'30'
 },
 {
   id:'Anxiety',
   value:'40'
 }
]


      },



      {
       name:'Bikramiditya Panda',
       img:'assets/video/thumb/mtngimg1.jpeg',
       userDtls:[
{
  id:'Sex',
  value:'Male'
},
{
  id:'Race',
  value:'Asian'
},
{
  id:'Age',
  value:'30-35'
},
{
  id:'Surprised',
  value:'10'
},
{
  id:'Angry',
  value:'10'
},
{
  id:'Sad',
  value:'40'
},
{
  id:'Happy',
  value:'20'
},
{
  id:'Neutral',
  value:'60'
},
{
  id:'Anxiety',
  value:'50'
}
       ]
     },
      {
       name:'Subhrajit Mohanty',
       img:'assets/video/thumb/mtngimg2.jpeg',
       userDtls:[
{
  id:'Sex',
  value:'Male'
},
{
  id:'Race',
  value:'Asian'
},
{
  id:'Age',
  value:'35-40'
},
{
  id:'Surprised',
  value:'20'
},
{
  id:'Angry',
  value:'10'
},
{
  id:'Sad',
  value:'30'
},
{
  id:'Happy',
  value:'10'
},
{
  id:'Neutral',
  value:'50'
},
{
  id:'Anxiety',
  value:'40'
}
       ]
     },
     {
       name:'Marut Pattanaik',
       img:'assets/video/thumb/mtngimg3.jpeg',
       userDtls:[
{
  id:'Sex',
  value:'Male'
},
{
  id:'Race',
  value:'Asian'
},
{
  id:'Age',
  value:'30-35'
},
{
  id:'Surprised',
  value:'0'
},
{
  id:'Angry',
  value:'30'
},
{
  id:'Sad',
  value:'40'
},
{
  id:'Happy',
  value:'0'
},
{
  id:'Neutral',
  value:'40'
},
{
  id:'Anxiety',
  value:'50'
}
       ]
     },
     {
      name:'unknown',
      img:'assets/images/s.jpg',
      userDtls:[
{
 id:'Sex',
 value:'unknown	'
},
{
 id:'Race',
 value:'0'
},
{
 id:'Age',
 value:'0'
},
{
 id:'Surprised',
 value:'0'
},
{
 id:'Angry',
 value:'0'
},
{
 id:'Sad',
 value:'0'
},
{
 id:'Happy',
 value:'0'
},
{
 id:'Neutral',
 value:'0'
},
{
 id:'Anxiety',
 value:'0'
}
      ]
    }
   ]
    this.readioSelected = this.videoFiles[0].id;
    this.poster = this.videoFiles[0].thumb;
    this.videoSources[0].src='assets/video/'+this.videoFiles[0].id+'.mp4';
    this.readioMulSelected = this.videoFiles[0].id;
    this.mulposter = this.videoFiles[0].thumb;
    this.mulvideoSources[0].src='assets/video/'+this.videoFiles[0].id+'.mp4';
    this.preetyCode =JSON.stringify(this.mulUserData, undefined, 4);
  }
  chngSlction(event){
    const chngedData = this.videoFiles.filter(element => element.id == this.readioSelected);
    this.poster = chngedData[0].thumb;
    this.videoSources[0].src='assets/video/'+chngedData[0].id+'.mp4';
    this.plyrBtn=true;
  }

  chngMulSlction(event){
    const chngedData = this.videoFiles.filter(element => element.id == this.readioMulSelected);
    this.mulposter = chngedData[0].thumb;
    this.mulvideoSources[0].src='assets/video/'+chngedData[0].id+'.mp4';
    this.mulPlyrBtn=true;

  }
  play(): void {
      this.player.play(); 
      this.plyrBtn=true;
  }
  mulPlay(): void {
    this.mulPlayer.play(); 
    this.mulPlyrBtn=true;
}
mulPause(): void {
  this.mulPlayer.pause(); 
  this.mulPlyrBtn=false;
}
  pause(): void {
      this.player.pause(); 
      this.plyrBtn=false;

  }
  audioEnded(){
    this.player.play(); 
    this.mulPlayer.play(); 
  }
  copyIndv(){
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.preetyCode;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.clipboardColor = "rgb(229 151 61)"
    setTimeout(() => {
    this.clipboardColor = "gainsboro";
    }, 1500);
  }
  copyMul(){
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.preetyCode;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.mulClipboardColor = "rgb(229 151 61)"
    setTimeout(() => {
    this.mulClipboardColor = "gainsboro";
    }, 1500);
  }
  copyModalIndv(){
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.preetyCode;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.clipboardModalColor = "rgb(229 151 61)"
    setTimeout(() => {
    this.clipboardModalColor = "gainsboro";
    }, 1500);
  }
  analyse(){

    this.preetyCode =JSON.stringify(this.mulUserData, undefined, 4)
}
  slideConfig = {slidesToShow: 4, slidesToScroll: 1, infinite:  false,
    responsive: [
      {
        breakpoint: 1280,
        settings: {
          draggable:  false,
          arrows:  true,
          slidesToShow: 3,
          slidesToScroll: 1,
          dots:  false,
        }
      },
      {
        breakpoint: 1024,
        settings: {
          draggable: false,
          arrows:  true,
          slidesToShow: 2,
          slidesToScroll: 1,
          dots:  false,
        }
      },
      {
        breakpoint: 541,
        settings: {
          draggable: true,
          slidesToShow: 1.1,
          slidesToScroll: 1,
          mobileFirst: true,
          arrows: false,
        }
      }
    ]
};

}

