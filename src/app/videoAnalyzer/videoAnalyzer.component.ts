import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import * as Plyr from 'plyr';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Chart } from 'chart.js';
import { TitleCasePipe } from '@angular/common';
import { Cognitive } from '../../service/cognitive.service';
import { ShowRoomDataObject } from './showroomDataObject';
import { CogProcessObj } from './cogProcessObj';
import { trigger, transition, style, animate, query, stagger } from '@angular/animations';

@Component({
  selector: 'app-videoAnalyzer',
  templateUrl: './videoAnalyzer.component.html',
  styleUrls: ['./videoAnalyzer.component.scss'],
  providers: [TitleCasePipe, Cognitive],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [ // each time the binding value changes
        query(':enter', [
          style({ opacity: 0 }),
          stagger(100, [
            animate('1s', style({ opacity: 1 }))
          ])
        ], { optional: true })
      ])
    ])
  ]
})
export class VideoAnalyzerComponent implements OnInit {
  public showRoomDataObject: ShowRoomDataObject;
  serviceData: any = [];
  display = 'none';
  color: string;
  errorDisplay: string = 'none';
  errorMessagefrmServ: any;
  messagetemp: any;
  onlineStatus: string;
  tenantType: string;
  appNm: any;
  colstudentData: any;
  public showroomUrl: string;
  public showroomAdminUrl: string;
  player: Plyr;
  videoSources: Plyr.Source[];
  options: Plyr.Options = {
    controls: ['play-large', 'play', 'progress', 'current-time', 'settings', 'fullscreen'],
    autoplay: false,
    clickToPlay: true,
  };
  poster: string;
  data: any;
  defaultTab: any;
  cogProcVidObj: CogProcessObj;
  filterForm: FormGroup;
  yearmodel: string;
  grademodel: string;
  courseModel: string;
  branchModel: string;
  sectionmodel: string;
  subjectModel: string;
  typeModel: string;
  academicYear: any;
  collegeJsonData: any;
  branchData: any;
  standardData: any;
  sectionData: any;
  subjectData: any;
  optionsChart: any;
  testData: { name: string; age: number; emotions: { Surprised: number; Fearful: number; Happy: number; Neutral: number; Angry: number; NA: number; Sad: number; }; race: string; gender: string; face_landmark: any[]; yawn_count: number; user_info_not_available_count: number; }[];
  participantTotal: number;
  tempOverallData: { ovrlHappy: number; ovrlNeutral: number; ovrlSad: number; ovrlFearful: number; ovrlSurprise: number; ovrlAngry: number; };
  vidCogDtls: any;
  overallData: { Angry: number; Fearful: number; Happy: number; Neutral: number; Sad: number; Surprised: number; };
  LineOption: { xAxis: { type: string; data: string[]; }; yAxis: { type: string; }; series: { data: number[]; type: string; smooth: boolean; }[]; };
  showloading: boolean = false;
  LineChartHealth: Chart;
  docId: any;
  slideConfig = {
    slidesToShow: 8, slidesToScroll: 1, infinite: false,
    responsive: [
      {
        breakpoint: 7681,
        settings: {
          draggable: false,
          arrows: true,
          slidesToShow: 10,
          slidesToScroll: 1,
          dots: false,
          mobileFirst: true,
        }
      },
      {
        breakpoint: 3840,
        settings: {
          draggable: false,
          arrows: true,
          slidesToShow: 10,
          slidesToScroll: 1,
          dots: false,
          mobileFirst: true,
        }
      },
      {
        breakpoint: 2560,
        settings: {
          draggable: false,
          arrows: true,
          slidesToShow: 10,
          slidesToScroll: 1,
          dots: false,
          mobileFirst: true,
        }
      },
      {
        breakpoint: 2048,
        settings: {
          draggable: false,
          arrows: true,
          slidesToShow: 10,
          slidesToScroll: 1,
          dots: false,
          mobileFirst: true,
        }
      },
      {
        breakpoint: 1920,
        settings: {
          draggable: false,
          arrows: true,
          slidesToShow: 9,
          slidesToScroll: 1,
          dots: false,
        }
      },
      {
        breakpoint: 1856,
        settings: {
          draggable: false,
          arrows: true,
          slidesToShow: 8,
          slidesToScroll: 1,
          dots: false,
        }
      },

      {
        breakpoint: 1680,
        settings: {
          draggable: false,
          arrows: true,
          slidesToShow: 8,
          slidesToScroll: 1,
          dots: false,
        }
      },
      {
        breakpoint: 1600,
        settings: {
          draggable: false,
          arrows: true,
          slidesToShow: 8,
          slidesToScroll: 1,
          dots: false,
        }
      },
      {
        breakpoint: 1540,
        settings: {
          draggable: false,
          arrows: true,
          slidesToShow: 7,
          slidesToScroll: 1,
          dots: false,
        }
      },
      {
        breakpoint: 1400,
        settings: {
          draggable: false,
          arrows: true,
          slidesToShow: 7,
          slidesToScroll: 1,
          dots: false,
        }
      },
      {
        breakpoint: 1367,
        settings: {
          draggable: false,
          arrows: true,
          slidesToShow: 7,
          slidesToScroll: 1,
          dots: false,
        }
      },



      {
        breakpoint: 1280,
        settings: {
          draggable: false,
          arrows: true,
          slidesToShow: 6,
          slidesToScroll: 1,
          dots: false,
        }
      },
      {
        breakpoint: 1152,
        settings: {
          draggable: false,
          arrows: true,
          slidesToShow: 6,
          slidesToScroll: 1,
          dots: false,
        }
      },

      {
        breakpoint: 1024,
        settings: {
          draggable: false,
          arrows: true,
          slidesToShow: 5,
          slidesToScroll: 5,
          dots: false,
        }
      },
      {
        breakpoint: 960,
        settings: {
          draggable: true,
          arrows: false,
          slidesToShow: 4.1,
          slidesToScroll: 4,
          dots: false,
        }
      },

      {
        breakpoint: 800,
        settings: {
          draggable: true,
          slidesToShow: 4.1,
          slidesToScroll: 4,
          arrows: false,
        }
      },
      {
        breakpoint: 720,
        settings: {
          draggable: true,
          slidesToShow: 4.1,
          slidesToScroll: 4,
          mobileFirst: true,
          arrows: false,
        }
      },
      {
        breakpoint: 640,
        settings: {
          slidesToShow: 3.1,
          slidesToScroll: 3,
          mobileFirst: true,
          arrows: false,
        }
      },
      {
        breakpoint: 541,
        settings: {
          draggable: true,
          slidesToShow: 3.1,
          slidesToScroll: 3,
          mobileFirst: true,
          arrows: false,
        }
      },
      {
        breakpoint: 481,
        settings: {
          draggable: true,
          slidesToShow: 2.1,
          slidesToScroll: 2,
          mobileFirst: true,
          arrows: false,
        }
      },
      {
        breakpoint: 360,
        settings: {
          draggable: true,
          slidesToShow: 2.1,
          slidesToScroll: 2,
          mobileFirst: true,
          arrows: false,
        }
      },
      {
        breakpoint: 321,
        settings: {
          draggable: true,
          slidesToShow: 1.1,
          slidesToScroll: 1,
          mobileFirst: true,
          arrows: false,
        }
      }
    ]
  };
  preetyCode: string;
  mulClipboardColor:any='gainsboro';
  emotionData: any;
  clipboardModalColor: string='gainsboro';

  constructor(private titlecasePipe: TitleCasePipe, private vidulyzerService: Cognitive) {
    this.tenantType = localStorage.getItem('tenantType');
  }

  ngOnInit() {
    this.vidCogDtls = [];
    this.defaultTab = "summary";
    this.tenantType = 'corp';
    this.onlineStatus = sessionStorage.getItem('offlineStatuss');
    this.cogProcVidObj = new CogProcessObj();
    this.cogProcVidObj.tenId = 'crb';
    this.vidulyzerService.getCogPrcVidService(this, this.cogProcVidObj);
  }
  // callback function of cogProcess Vid
  getCogPrcVidSucc(data) {
    this.serviceData = data.result.vids;
    this.errorMessagefrmServ = null;
    this.errorDisplay = 'none';
    this.serviceData.length != 0 ? this.vidDtls(this.serviceData[0]) : this.vidCogDtls = [];
  }

  tabChange(src) {
    this.defaultTab = src;
    if (src == 'detailsAll') {
      this.vidulyzerService.getTimeSeries(this, 'https://api.vidulearn.net:8003/get_vidcog_timeseries?doc_id=' + this.docId)
    }else{
      this.emotionData != undefined ? this.preetyCode = JSON.stringify(this.emotionData, undefined, 4) : null;
    }
  }
  vidDtls(src) {
    var vdReqData = {
      "src": "all",
      "type": "",
      "tenId": 'crb',
      "uid": 'crb.admin',
      "vid": src.id
    }
    if (this.LineChartHealth !== undefined) {
      this.LineChartHealth.destroy();
    }
    this.defaultTab = "summary";
    this.vidulyzerService.getVideos(this, vdReqData);
    this.vidulyzerService.getEmotion(this, "https://api.vidulearn.net:8003/get_vidcog_det?request_id=req-" + src.id)

  }
  getVideosSucc(src) {
    this.videoSources = [{
      src: src.result.video.extId_csp,
      provider: 'vimeo',
      size: 720
    }];
    this.docId = src.result.video.docId;
  }

  getEmotionSucc(src) {
    this.errorDisplay = "none";
    this.errorMessagefrmServ = null;
    var tempvidCogDtls = src._source.user_details;
    this.emotionData = src._source.user_details;
    this.preetyCode = JSON.stringify(tempvidCogDtls, undefined, 4);
    var vidCogTotalDtls = [];
    var ovlemotions = {
      Angry: 0,
      Fearful: 0,
      Happy: 0,
      Neutral: 0,
      Sad: 0,
      Surprised: 0
    }

    for (let i = 0; i < tempvidCogDtls.length; i++) {
      var userData = {
        age: "",
        gender: "",
        name: "",
        race: "",
        yawn_count: "",
        emotions: {
          Angry: 0,
          Fearful: 0,
          Happy: 0,
          Neutral: 0,
          Sad: 0,
          Surprised: 0
        },
        totalEmotions: 0
      }
      userData.age = tempvidCogDtls[i].age;
      userData.gender = tempvidCogDtls[i].gender;
      userData.name = tempvidCogDtls[i].name;
      userData.race = tempvidCogDtls[i].race;
      userData.yawn_count = tempvidCogDtls.yawn_count == undefined ? 0 : tempvidCogDtls.yawn_count;
      userData.emotions.Happy = tempvidCogDtls[i].emotions.hasOwnProperty("Happy") ? tempvidCogDtls[i].emotions.Happy : 0;
      userData.emotions.Angry = tempvidCogDtls[i].emotions.hasOwnProperty("Angry") ? tempvidCogDtls[i].emotions.Angry : 0;
      userData.emotions.Fearful = tempvidCogDtls[i].emotions.hasOwnProperty("Fearful") ? tempvidCogDtls[i].emotions.Fearful : 0;
      userData.emotions.Sad = tempvidCogDtls[i].emotions.hasOwnProperty("Sad") ? tempvidCogDtls[i].emotions.Sad : 0;
      userData.emotions.Surprised = tempvidCogDtls[i].emotions.hasOwnProperty("Surprised") ? tempvidCogDtls[i].emotions.Surprised : 0;
      userData.emotions.Neutral = tempvidCogDtls[i].emotions.hasOwnProperty("Neutral") ? tempvidCogDtls[i].emotions.Neutral : 0;
      userData.totalEmotions = Math.round(userData.emotions.Happy + userData.emotions.Angry + userData.emotions.Fearful + userData.emotions.Sad + userData.emotions.Surprised + userData.emotions.Neutral)
      ovlemotions.Happy = userData.emotions.Happy + ovlemotions.Happy;
      ovlemotions.Angry = userData.emotions.Angry + ovlemotions.Angry;
      ovlemotions.Fearful = userData.emotions.Fearful + ovlemotions.Fearful;
      ovlemotions.Neutral = userData.emotions.Neutral + ovlemotions.Neutral;
      ovlemotions.Sad = userData.emotions.Sad + ovlemotions.Sad;
      ovlemotions.Surprised = userData.emotions.Surprised + ovlemotions.Surprised;
      vidCogTotalDtls.push(userData);
    }
    for (let j = 0; j < vidCogTotalDtls.length; j++) {
      vidCogTotalDtls[j].emotions.Happy = Math.round((vidCogTotalDtls[j].emotions.Happy * 100) / vidCogTotalDtls[j].totalEmotions);
      vidCogTotalDtls[j].emotions.Angry = Math.round((vidCogTotalDtls[j].emotions.Angry * 100) / vidCogTotalDtls[j].totalEmotions);
      vidCogTotalDtls[j].emotions.Fearful = Math.round((vidCogTotalDtls[j].emotions.Fearful * 100) / vidCogTotalDtls[j].totalEmotions)
      vidCogTotalDtls[j].emotions.Sad = Math.round((vidCogTotalDtls[j].emotions.Sad * 100) / vidCogTotalDtls[j].totalEmotions)
      vidCogTotalDtls[j].emotions.Surprised = Math.round((vidCogTotalDtls[j].emotions.Surprised * 100) / vidCogTotalDtls[j].totalEmotions)
      vidCogTotalDtls[j].emotions.Neutral = Math.round((vidCogTotalDtls[j].emotions.Neutral * 100) / vidCogTotalDtls[j].totalEmotions)
    }
    var totalOveralEmot = ovlemotions.Happy + ovlemotions.Angry + ovlemotions.Fearful + ovlemotions.Neutral + ovlemotions.Sad + ovlemotions.Surprised;
    ovlemotions.Happy = Math.round((ovlemotions.Happy / totalOveralEmot) * 100);
    ovlemotions.Angry = Math.round((ovlemotions.Angry / totalOveralEmot) * 100);
    ovlemotions.Fearful = Math.round((ovlemotions.Fearful / totalOveralEmot) * 100);
    ovlemotions.Neutral = Math.round((ovlemotions.Neutral / totalOveralEmot) * 100);
    ovlemotions.Surprised = Math.round((ovlemotions.Surprised / totalOveralEmot) * 100);
    ovlemotions.Sad = Math.round((ovlemotions.Sad / totalOveralEmot) * 100);
    this.overallData = ovlemotions;
    this.vidCogDtls = vidCogTotalDtls;
    this.participantTotal = this.vidCogDtls.length;
    var tempYwn = [
      {
        id: "req-dfd1e228-7108-4f8f-b0d9-b5eeb2f7d952",
        dtls: [{
          nme: "unknown",
          ywn: 0
        }, {
          nme: "sujit",
          ywn: 0
        },
        {
          nme: "jos",
          ywn: 10
        },
        {
          nme: "subhrajit",
          ywn: 0
        }, {
          nme: "rock",
          ywn: 13
        }, {
          nme: "marut",
          ywn: 8
        }]
      },
      {
        id: "req-368c46f6-c350-4ade-8b7d-5bc1223510c11",
        dtls: [{
          nme: "unknown",
          ywn: 0
        },
        {
          nme: "jos",
          ywn: 18
        },
        {
          nme: "rock",
          ywn: 0
        }]
      },
      {
        id: "req-72b30445-37a5-4697-88c9-65ebbb3feb33",
        dtls: [{
          nme: "unknown",
          ywn: 0
        },
        {
          nme: "jenny",
          ywn: 5
        },
        {
          nme: "jos",
          ywn: 16
        },
        {
          nme: "rock",
          ywn: 0
        },
        {
          nme: "kamala",
          ywn: 10
        }]
      },
      {
        id: "req-c0aadc09-d932-4b21-8770-fa38dbdegg451",
        dtls: [{
          nme: "unknown",
          ywn: 0
        },
        {
          nme: "jos",
          ywn: 17
        },
        {
          nme: "david",
          ywn: 0
        },
        {
          nme: "rock",
          ywn: 0
        },
        {
          nme: "annie",
          ywn: 4
        }]
      }
    ]

    var finalYwn = tempYwn.filter(element => src._id == element.id);
    for (let n = 0; n < this.vidCogDtls.length; n++) {
      if (this.vidCogDtls[n].name == finalYwn[0].dtls[n].nme) {
        this.vidCogDtls[n].yawn_count = finalYwn[0].dtls[n].ywn
      }
    }
  }

  detailsAll(src) {
    var rawData = src;
    var yLabels = {
      1: 'Angry',
      2: 'Sad',
      3: 'Fearful',
      4: 'Neutral',
      5: 'Surprised',
      6: 'Happy'
    }
    var finalAry = [];
    var tempCol = [

      {
        brdrCol: '#158000',
        backCol: '#15800078'
      },
      {
        brdrCol: '#4c6ef5',
        backCol: '#4c6ef570'
      },
      {
        brdrCol: '#d2c021',
        backCol: '#d2c02182'
      },
      {
        brdrCol: '#fb7b04',
        backCol: '#fb7b047d'
      }, {
        brdrCol: '#000033',
        backCol: '#00003390'
      },
      {
        brdrCol: '#7c7771',
        backCol: '#7c777170'
      },
      {
        brdrCol: '#6ee953f5',
        backCol: '#6ee9538a'
      },
      {
        brdrCol: '#597691',
        backCol: '#5976917d'
      }
    ]

    for (let i = 0; i < rawData.length; i++) {
      var o = Math.round, r = Math.random, s = 215;
      var tempcolor = 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',' + r().toFixed(1) + ')';
      var lstIndx = tempcolor.lastIndexOf(",");
      var tempDatasets = {
        label: this.titlecasePipe.transform(rawData[i].user),
        borderColor: tempCol.length >= i ? tempCol[i].brdrCol : tempcolor.substring(0, lstIndx) + ')',
        fill: false,
        data: rawData[i].details,
        lineTension: 0,
        showLine: false,
        pointRadius: 7,
        pointBackgroundColor: tempCol.length >= i ? tempCol[i].backCol : tempcolor.substring(0, lstIndx) + ',0.7)',
      }
      finalAry.push(tempDatasets)
    }
    var finalUser = {
      type: "line",
      plugins: [{
        beforeInit: function(chart, options) {
          chart.legend.afterFit = function() {
            this.height = this.height + 20;
          };
        }
      }],
      data: {
        datasets: finalAry
      },
      options: {
        tooltips: {
          enabled: true,
          mode: 'single',
          callbacks: {
            label: function (tooltipItem, data) {
              var label = data.datasets[tooltipItem.datasetIndex].label || '';
              if (label) {
                label += ': ';
              }
              label += yLabels[tooltipItem.value];
              return label;
            },
            title: (tooltipItems, data) => {
              // return realLabel + "TOOLTIP";
              return new Date(tooltipItems[0].xLabel * 1000).toISOString().substr(11, 8);
            }
          }
        },
        responsive: true,
        bezierCurve: false,
        title: {
          display: false
        },
        scales: {
          xAxes: [
            {
              type: "linear",
              position: "bottom",
              ticks: {
                userCallback: function (tick) {
                  return new Date(tick * 1000).toISOString().substr(11, 8);
                }
              },
              scaleLabel: {
                labelString: "Time",
                display: true
              }
            }
          ],
          yAxes: [
            {
              type: "linear",
              ticks: {
                userCallback: function (label, index, labels) {
                  if (Math.floor(label) === label) {
                    return yLabels[label];
                  }
                }
              },
              scaleLabel: {
                labelString: "Mood",
                display: true
              }
            }
          ]
        }
      }
    }
    this.LineChartHealth = new Chart('lineChartHealth', finalUser);
  }
  getTimeSeriesSucc(src) {
    this.preetyCode = JSON.stringify(src, undefined, 4);
    this.detailsAll(src)
  }
  copyMul(){
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.preetyCode;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.mulClipboardColor = "rgb(229 151 61)";
    setTimeout(() => {
    this.mulClipboardColor = "gainsboro";
    }, 1500);
  }
  copyModalIndv(){
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.preetyCode;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.clipboardModalColor = "rgb(229 151 61)"
    setTimeout(() => {
    this.clipboardModalColor = "gainsboro";
    }, 1500);
  }

}
