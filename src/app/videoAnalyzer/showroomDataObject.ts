export class ShowRoomDataObject {

  public std: string;
  public tenId: string;
  public uid: string;
  public urole: string;
  public branch: string;
  public course: string;
  public sem: string;
  public type: string;
  public vidCat: string;
}
