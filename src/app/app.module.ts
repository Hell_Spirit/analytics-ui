import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { routing } from './app.routing';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FileUploaderComponent } from './file-uploader/file-uploader.component';
import { ProgressBarModule } from "angular-progress-bar";
import { HighlightJsModule } from 'ngx-highlight-js';
import { VideoAnalysisComponent } from './videoAnalysis/videoAnalysis.component';
import { PlyrModule } from 'ngx-plyr';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { MultiFileUploaderComponent } from './multifile-uploader/multifile-uploader.component';
import { ModalModule } from 'ngx-modal';
import { VideoEmotionChart } from './videoEmotionChart/videoEmotionChart.component';
import { VideoAnalyzerComponent } from './videoAnalyzer/videoAnalyzer.component';
import { ChartModule } from 'angular2-chartjs';
import { NgxEchartsModule } from 'ngx-echarts';
import { HttpClientModule } from '@angular/common/http';
import { NgHttpLoaderModule } from 'ng-http-loader';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    routing,
    ProgressBarModule,
    HighlightJsModule,
    PlyrModule,
    SlickCarouselModule,
    ModalModule,
    ChartModule,
    ReactiveFormsModule,
    NgxEchartsModule,
    HttpClientModule,
    NgHttpLoaderModule
  ],
  declarations: [
    AppComponent,
    FileUploaderComponent,
    VideoAnalysisComponent,
    MultiFileUploaderComponent,
    VideoEmotionChart,
    VideoAnalyzerComponent
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: './' }
  ]
})
export class AppModule { }
