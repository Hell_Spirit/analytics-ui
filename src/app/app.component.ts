import { Component, OnInit} from '@angular/core';
import { Cognitive } from '../service/cognitive.service';
import { Spinkit } from 'ng-http-loader';
import { SpinnerVisibilityService } from 'ng-http-loader';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [Cognitive]
})
export class AppComponent implements OnInit {
  checkedItem: { id: string; checked: boolean }[];
  public spinkit = Spinkit;
  url: string | ArrayBuffer;
  mulUserData: { name: string; img: string; userDtls: { id: string; value: string; }[]; }[];
  userEmotionData: { id: string; value: string; }[];
  prettyCode: string = 'No Data Available';
  mulPreetyCode: string;
  modalPrettyCode: string = "No Data Available";
  clipboardColor: any = 'gainsboro';
  clipboardModalColor: any = 'gainsboro';
  mulClipboardColor: any = 'gainsboro';
  slideConfig = {
    slidesToShow: 2, slidesToScroll: 1, infinite: false,
  };
  singleSlideConfig = {
    slidesToShow: 2, slidesToScroll: 1, infinite: false,
  };
  tempImgSrc: any;
  analyzeImg: any;
  userDetails: any;
  errorDisplay: string = "none";
  errorMessagefrmServ: any;
  length: number = 0;
  constructor(private vidulyzerService: Cognitive, public spinner: SpinnerVisibilityService) { }

  ngOnInit() {
    this.checkedItem = [
      {
        id: 'Age',
        checked: true
      },
      {
        id: 'Race',
        checked: true
      },
      {
        id: 'Gender',
        checked: true
      },
      {
        id: 'Emotion',
        checked: true
      },
      {
        id: 'Landmark',
        checked: true
      }

    ];
  }
  analyse() {
    this.tempImgSrc != undefined ? this.analyzeImg = this.tempImgSrc.fileSrc : null;
    var checkedData = this.checkedItem.filter(item => item.checked);
    if (checkedData.length != 0 && this.tempImgSrc != undefined) {
      this.spinner.show();
      this.errorMessagefrmServ = null;
      this.errorDisplay = "none";
      var finalData = [];
      for (let i = 0; i < checkedData.length; i++) {
        finalData.push(checkedData[i].id.toLowerCase())
      }
      const formData = new FormData();
      formData.append('image_object', this.tempImgSrc.fileInfo);
      formData.append('request_apis', JSON.stringify(finalData));
      this.vidulyzerService.imageDetection(this, formData);
    } else {
      if (this.tempImgSrc == undefined) {
        this.errorMessagefrmServ = 'Please Upload A File';
        this.errorDisplay = "block";
      } else {
        this.errorDisplay = "block";
        this.errorMessagefrmServ = 'Please Select Any Emotion To Analyze';
      }
    }


  }
  imageDetectionSucc(src) {
    this.spinner.hide();
    if (src != null) {
      this.prettyCode = JSON.stringify(src, undefined, 4);
      this.modalPrettyCode = JSON.stringify(src, undefined, 4);
      var tempUsersDtls = [];
      for (let i = 0; i < src.user_details.length; i++) {
        var userDetails = {
          'nme': src.user_details[i].user,
          'unknw': src.user_details[i].user.includes('unknown'),
          'userDtls': [{
            'nme': 'age',
            'value': src.user_details[i].hasOwnProperty("age") ? src.user_details[i].age : '-'
          },
          {
            'nme': 'gender',
            'value': src.user_details[i].hasOwnProperty("gender") ? src.user_details[i].gender == 'M' ? 'Male' : 'Female' : '-'
          },
          {
            'nme': 'race',
            'value': src.user_details[i].hasOwnProperty("race") ? src.user_details[i].race : '-'
          },
          {
            'nme': 'emotion',
            'value': src.user_details[i].hasOwnProperty("emotion") ? src.user_details[i].emotion : '-'
          }]
        }
        tempUsersDtls.push(userDetails)
      }
      this.userDetails = tempUsersDtls;
      this.length = this.userDetails.length;
      this.length == 1 ? this.slideConfig.slidesToShow = 1 : this.slideConfig.slidesToShow = 2;
    } else {
      this.userDetails = [];
      this.prettyCode = 'No Data Available';
      this.length = 0;
      this.modalPrettyCode = 'No Data Available';
    }
  }
  analyseMul() {
    this.mulPreetyCode = JSON.stringify(this.mulUserData, undefined, 4);
  }

  copyIndv() {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.prettyCode;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.clipboardColor = "rgb(229 151 61)"
    setTimeout(() => {
      this.clipboardColor = "gainsboro";
    }, 1500);
  }
  copyMul() {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.prettyCode;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.mulClipboardColor = "rgb(229 151 61)"
    setTimeout(() => {
      this.mulClipboardColor = "gainsboro";
    }, 1500);
  }
  copyModalIndv() {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.prettyCode;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.clipboardModalColor = "rgb(229 151 61)"
    setTimeout(() => {
      this.clipboardModalColor = "gainsboro";
    }, 1500);
  }

  public outputEvent(src: any): void {
    this.tempImgSrc = src;
    this.errorMessagefrmServ = null;
    this.errorDisplay = "none";
  }
  globalError() {
    this.spinner.hide();
    this.errorDisplay = "block";
    this.errorMessagefrmServ = 'Please Contact Your System Administrator';
  }

}


