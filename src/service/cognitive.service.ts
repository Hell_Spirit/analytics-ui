import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class Cognitive {
  public data;
  public requestBody: any;

  constructor(private http: HttpClient) {
  }

  getCogPrcVid(data) {
    this.requestBody = data;
    return this.http.post('https://api.vidulearn.net:8014/vdl/admin/cogProcVids', this.requestBody, { observe: 'response' })
  }
  getCogPrcVidService(callback, data) {
    this.getCogPrcVid(data).subscribe(resp => {
      var homedata = resp.body;
      callback.getCogPrcVidSucc(homedata);
    },
      error => {
        callback.globalError(error);
      }
    );
  }


  getVideosService(data) {
    this.requestBody = data;
    return this.http.post('https://api.vidulearn.net:8014/vdl/common/getVidDet', this.requestBody, { observe: 'response' })
  }
  getVideos(callback, data) {
    this.getVideosService(data).subscribe(resp => {
      var homedata = resp.body;
      callback.getVideosSucc(homedata);
    },
      error => {
        callback.globalError(error);
      }
    );
  }



  getEmotionData(url) {
    return this.http.get(url, { observe: 'response' })
  }
  getEmotion(callback, url) {
    this.getEmotionData(url).subscribe(resp => {
      var homedata = resp.body;
      callback.getEmotionSucc(homedata);
    },
      error => {
        callback.globalError(error);
      }
    );
  }

  getTimeSeriesData(url) {
    return this.http.get(url, { observe: 'response' })
  }
  getTimeSeries(callback, url) {
    this.getTimeSeriesData(url).subscribe(resp => {
      var homedata = resp.body;
      callback.getTimeSeriesSucc(homedata);
    },
      error => {
        callback.globalError(error);
      }
    );
  }


  imageDetectionService(data) {
    this.requestBody = data;
    return this.http.post('http://192.168.50.32:8002/get_selected_apis', this.requestBody, { observe: 'response' })
  }
  imageDetection(callback, data) {
    this.imageDetectionService(data).subscribe(resp => {
      var homedata = resp.body;
      callback.imageDetectionSucc(homedata);
    },
      error => {
        callback.globalError(error);
      }
    );
  }

}
